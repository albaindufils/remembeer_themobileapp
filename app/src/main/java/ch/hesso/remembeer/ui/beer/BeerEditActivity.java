package ch.hesso.remembeer.ui.beer;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.lifecycle.ViewModelProviders;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.slider.Slider;

import java.io.IOException;

import ch.hesso.remembeer.BaseActivity;
import ch.hesso.remembeer.R;
import ch.hesso.remembeer.adapter.BreweryAdapter;
import ch.hesso.remembeer.database.entity.BeerEntity;
import ch.hesso.remembeer.database.entity.BreweryEntity;
import ch.hesso.remembeer.utils.Helpers;
import ch.hesso.remembeer.utils.OnAsyncEventListener;
import ch.hesso.remembeer.viewmodel.beer.BeerViewModel;
import ch.hesso.remembeer.viewmodel.brewery.BreweryListViewModel;

/**
 * Activite pour la page "Modification" pour la liste des bieres
 * Etend la classe BaseActivity
 */
public class BeerEditActivity extends BaseActivity {
    private static final String TAG = "BeerEditActivity";

    private String beerId;
    private BeerEntity beer = null;
    private EditText editName, editDesc, editCapacity, editAcidity, editAlcool;
    private BreweryEntity brewery = null;
    private ProgressBar progressBar;
    private AutoCompleteTextView breweryDropdown;
    private ImageView beerPhoto;
    private BeerViewModel beerViewModel;
    private BreweryListViewModel breweryListViewModel;
    // private boolean createBeer = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beer_edit);
        getLayoutInflater().inflate(R.layout.activity_beer_edit, frameLayout);
        setTitle(getString(R.string.beer_edit));

        editName = findViewById(R.id.edit_name);
        editDesc = findViewById(R.id.edit_desc);
        // editFrom = findViewById(R.id.edit_from);
        beerPhoto = findViewById(R.id.beer_photo);
        editCapacity = findViewById(R.id.edit_capacity);
        editAlcool = findViewById(R.id.edit_alcool);
        editAlcool.setInputType(InputType.TYPE_CLASS_NUMBER);
        breweryDropdown = findViewById(R.id.brewery_dropdown);
        navigationView.setCheckedItem(position);


        beerId = getIntent().getStringExtra("beerId");

        if(beerId == null) {
            beer = new BeerEntity();
            beerId = "";
        }

        Log.d(TAG, "clicked on beerId: " + beerId);

        BeerViewModel.Factory factory = new BeerViewModel.Factory(getApplication(), beerId);
        beerViewModel = ViewModelProviders.of(this, factory).get(BeerViewModel.class);
        beerViewModel.getBeer().observe(this, beer -> {
            if(beer != null) {
                this.beer = beer;
                initViewData();
            }
        });

    }

    /**
     * Methode pour attribuer les informations aux différents champs
     */
    public void initViewData() {
        BreweryListViewModel.Factory factoryBrewey = new BreweryListViewModel.Factory(getApplication());
        breweryListViewModel = ViewModelProviders.of(this, factoryBrewey).get(BreweryListViewModel.class);
        breweryListViewModel.getBreweries().observe(this, breweries -> {
            if(breweries != null) {
                // Update dropdown
                // breweryId = beer.getFrom();
                breweryDropdown.clearListSelection();
                BreweryAdapter adapter =
                        new BreweryAdapter(
                                this,
                                R.layout.dropdown_brewery_item,
                                breweries,
                                breweryDropdown);
                breweryDropdown.setAdapter(adapter);
                breweryDropdown.setFocusableInTouchMode(false);
                breweryDropdown.setOnItemClickListener((parent1, view, data_index, id) -> {
                    brewery = breweries.get(data_index);
                    breweryDropdown.setText(brewery.getName(), false);
                });
                if (beer.getBreweryId() != null) {
                    for (BreweryEntity b: breweries) {
                        if(beer.getBreweryId().equals(b.getIdBrewery())) {
                            brewery = b;
                            breweryDropdown.setText(brewery.getName(),false);
                        }

                    }
                }
            }
        });
        editName.setText(beer.getName());
        editDesc.setText(beer.getDescription());
        editAlcool.setText("" + beer.getAlcool());
        editCapacity.setText("" + beer.getCapacity());

        // editFrom.setText(beer.getFrom());
        initImageBeer();
    }

    private void initImageBeer() {
        Helpers.setImageFromFirebaseOrDefault(beerPhoto, beer.getImage(), beer, this);
    }

    private void updateBeerImage(String path) {
        beer.setImage(path);
//        beerViewModel.update(beer, new OnAsyncEventListener() {
//            @Override
//            public void onSuccess() {
//                Log.d(TAG, "update: success");
//            }
//
//            @Override
//            public void onFailure(Exception e) {
//                Log.d(TAG, "update: fail");
//            }
//        });
        initImageBeer();
    }

    /**
     * Methode pour verification des champs
     */
    private boolean isFormValid() {
        boolean failed = false;
        if(editName.getText().toString().trim().length() == 0) {
            failed = true;
            editName.setError( "Valeur obligatoire" );
        }

        if(editDesc.getText().toString().trim().length() == 0) {
            failed = true;
            editDesc.setError( "Valeur obligatoire" );
        }

        if(editCapacity.getText().toString().trim().length() == 0) {
            failed = true;
            editCapacity.setError( "Valeur obligatoire" );
        }

        if(editAlcool.getText().toString().trim().length() == 0) {
            failed = true;
            editAlcool.setError( "Valeur obligatoire" );
        }

        if(brewery == null) {
            failed = true;
            breweryDropdown.setError( "Valeur obligatoire" );
        }

        return !failed;
    }

    /**
     * Methode pour attribuer les nouvelles valeurs choisies par le user
     */
    public void saveCliqued(View view) {
        Log.d("BeerEditActivity", "cliqued");

        if(isFormValid()){
            beer.setName(editName.getText().toString());
            beer.setDescription(editDesc.getText().toString());
            beer.setAlcool(Double.parseDouble(editAlcool.getText().toString()));
            beer.setCapacity(Double.parseDouble(editCapacity.getText().toString()));
            beer.setBreweryId(brewery.getIdBrewery());
            if(beerId == null || beerId.equals("")) {
                beerViewModel.createBeer(beer, new OnAsyncEventListener() {
                    @Override
                    public void onSuccess() {
                        Log.d(TAG, "createBeer: success");
                        finish();
                    }

                    @Override
                    public void onFailure(Exception e) {
                        Log.d(TAG, "createBeer: fail");
                    }
                });
            } else {
                beerViewModel.update(beer, new OnAsyncEventListener() {
                    @Override
                    public void onSuccess() {
                        Log.d(TAG, "update: fail");
                        finish();
                    }

                    @Override
                    public void onFailure(Exception e) {
                        Log.d(TAG, "update: fail");
                    }
                });
            }
        }
    }

    /**
     * Methode pour modifier l'image
     * Propose plusieurs choix (prendre la photo, importation..)
     */
    public void changePicture(View view) {
        final CharSequence[] options = { "Prendre une photo", "Choisir depuis la gallerie","Annuler" };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose your profile picture");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Prendre une photo")) {
                    Intent takePicture = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePicture, 0);

                } else if (options[item].equals("Choisir depuis la gallerie")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, 1);

                } else if (options[item].equals("Annuler")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        OnCompleteListener<Uri> onCompleteListener = new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                Log.d("onCompleteListener", task.getResult().getLastPathSegment());
                updateBeerImage(task.getResult().getLastPathSegment());
            }
        };
        if (resultCode != RESULT_CANCELED) {
            switch (requestCode) {
                case 0:
                    if (resultCode == RESULT_OK && data != null) {
                        Bitmap image = (Bitmap) data.getExtras().get("data");
                        Helpers.saveBitmapToFirebase(image, onCompleteListener);

                    }
                    break;
                case 1:
                    if (resultCode == RESULT_OK && data != null) {
                        try {
                            Uri imageUri = data.getData();
                            Bitmap image = MediaStore.Images.Media.getBitmap(this.getContentResolver(),
                                    imageUri);
                            Helpers.saveBitmapToFirebase(image, onCompleteListener);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                }

            }

    }


}