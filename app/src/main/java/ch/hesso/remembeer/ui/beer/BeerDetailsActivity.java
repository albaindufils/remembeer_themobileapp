package ch.hesso.remembeer.ui.beer;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import ch.hesso.remembeer.BaseActivity;
import ch.hesso.remembeer.R;
import ch.hesso.remembeer.adapter.BreweryAdapter;
import ch.hesso.remembeer.database.entity.BeerEntity;
import ch.hesso.remembeer.database.entity.BreweryEntity;
import ch.hesso.remembeer.utils.Helpers;
import ch.hesso.remembeer.utils.OnAsyncEventListener;
import ch.hesso.remembeer.viewmodel.beer.BeerViewModel;
import ch.hesso.remembeer.viewmodel.brewery.BreweryViewModel;

/**
 * Activite pour la page "Details" pour la biere choisie
 * Etend la classe BaseActivity
 */
public class BeerDetailsActivity extends BaseActivity {
    private static final String TAG = "BeerDetailsActivity";

    private String beerId;
    private BeerEntity beer;
    private BreweryEntity brewery;
    private TextView tvName, txt_description, txt_from, txt_capacity, txt_alcool;
    private BeerViewModel beerViewModel;
    private ProgressBar progressBar;
    private FloatingActionButton fl_fav;
    private ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_beer_details, frameLayout);
        setTitle(getString(R.string.beer_details));

        tvName = findViewById(R.id.tvNameBeer);
        txt_alcool = findViewById(R.id.text_alcool);
        txt_from = findViewById(R.id.text_from);
        txt_capacity = findViewById(R.id.text_capacite);
        txt_description = findViewById(R.id.txt_description);
        image = findViewById(R.id.image_beer);
        fl_fav = findViewById(R.id.fl_like);
        progressBar = findViewById(R.id.loading_spinner);
        progressBar.setVisibility(View.VISIBLE);

        navigationView.setCheckedItem(position);
        beerId = getIntent().getStringExtra("beerId");
        // isFavoris = fav_list.getBoolean(String.valueOf(beerId), false);
        // isFavoris = fav_list.getLong(String.valueOf(beerId), beerId) != null;
        updateOrGetBeer();


    }
    private void updateOrGetBeer() {
        BeerViewModel.Factory factory = new BeerViewModel.Factory(getApplication(), beerId);
        beerViewModel = ViewModelProviders.of(this, factory).get(BeerViewModel.class);
        beerViewModel.getBeer().observe(this, beer -> {
            if(beer != null) {
                this.beer = beer;
                this.initViewData();
                progressBar.setVisibility(View.GONE);
            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
        updateOrGetBeer();
    }

    private void setFavorisButton() {
        fl_fav.setImageResource(beer.isFavoris()?R.drawable.like_black_full:R.drawable.like_black_empty);
    }

    public void initViewData() {
//        if(beer == null) {
//            return;
//        }
        BreweryViewModel.Factory factoryBrewery = new BreweryViewModel.Factory(getApplication(),
                beer.getBreweryId());
        BreweryViewModel breweryViewModel = ViewModelProviders.of(this, factoryBrewery).get(BreweryViewModel.class);
        breweryViewModel.getBrewery().observe(this, brewery -> {
            if(brewery != null) {
                this.brewery = brewery;
                txt_from.setText(brewery.getName());
                progressBar.setVisibility(View.GONE);

            }
        });
        setFavorisButton();
        tvName.setText(beer.getName());
        txt_capacity.setText(String.format("%s L" , "" + beer.getCapacity()));
        txt_alcool.setText(String.format("%s %%", "" + beer.getAlcool()));
        txt_description.setText(beer.getDescription());
        Helpers.setImageFromFirebaseOrDefault(image, beer.getImage(), beer,this);
    }

    /**
     * Methode pour lancer l'activite BeerEdit
     */
    public void editBeer(View view) {
        Log.d("BeerEditActivity", "edit");
        Intent intent = new Intent(this, BeerEditActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.putExtra("beerId", beerId);
        startActivity(intent);
    }


    /**
     * Methode pour supprimer un objet biere
     */
    public void deleteBeer(View view) {
        new MaterialAlertDialogBuilder(this)
                .setTitle(getString(R.string.beer_delete_title))
                .setMessage(getString(R.string.beer_delete_message, beer.getName()))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Continue with delete operation
                        Log.d(TAG, "Delete event");
                        beerViewModel.delete(beer, new OnAsyncEventListener() {
                            @Override
                            public void onSuccess() {
                                Log.d(TAG, "deleteBeer: success");
                            }

                            @Override
                            public void onFailure(Exception e) {
                                Log.d(TAG, "deleteBeer: epic fail");
                            }
                        });
                        finish();
                    }
                })
                // A null listener allows the button to dismiss the dialog and take no further action.
                .setNegativeButton(android.R.string.no, null)
                .setIcon(R.drawable.delete_yellow)
                .show();
    }



    public void setLikeBeer(View view) {
        beer.setFavoris(!beer.isFavoris());
        setFavorisButton();

        beerViewModel.update(beer, new OnAsyncEventListener() {
            @Override
            public void onSuccess() {
                Log.d(TAG, "update: success");
            }

            @Override
            public void onFailure(Exception e) {
                Log.d(TAG, "update: epic fail");
                beer.setFavoris(!beer.isFavoris());
            }
        });
    }

}
