package ch.hesso.remembeer.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import androidx.annotation.NonNull;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import ch.hesso.remembeer.BaseActivity;
import ch.hesso.remembeer.R;
import ch.hesso.remembeer.ui.beer.BeersActivity;
/**
 * Activite pour la page Home
 */
public class HomeActivity extends BaseActivity {

    private static final String TAG = "HOME_ACTIVITY";
    private final String FCM_API = "https://fcm.googleapis.com/fcm/send";
    private final String serverKey = "key=AAAAp0TuA14:APA91bGgyfKG_hBGnuflj25vzGpXDlP32t5npNudiOjSoxi48odf7mYCcrW9hn2Qd-Wpve5ZzWDxWsz2auHUyRfcNFwgYtzCWZC5iEg1rfBPT7mR2rHyUn7OeRLyWlbDpGpNaa8K7L8N";
    private final String contentType = "application/json";
    ImageButton search_btn;

    public HomeActivity() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_home, frameLayout);
        setTitle(getString(R.string.menu_home));
        navigationView.setCheckedItem(position);
        search_btn =(ImageButton)findViewById(R.id.search_btn);
        // Manage Firebase
        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "Fetching FCM registration token failed", task.getException());
                            return;
                        }

                        // Get new FCM registration token
                        String token = task.getResult();

                        // Log and toast
                        String msg = getString(R.string.msg_token_fmt, token);
                        Log.d(TAG, msg);
                        // Toast.makeText(HomeActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                });
        sendNotificationToEntity(null);
    }

    /**
     * Renvoi a la page SearchActivity lorsque le user clique sur la loupe
     */
    public void goBeers(View view){
        Intent intent = new Intent(this, BeersActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    public void sendNotificationToEntity(Object entity) {
        FirebaseMessaging.getInstance().subscribeToTopic("/topics/Enter_topic");

        String topic = "/topics/Enter_topic"; //topic has to match what the receiver

        JSONObject notification = new JSONObject();
        JSONObject notifcationBody = new JSONObject();

        try {
            notifcationBody.put("title", "Nouvel utilisateur connecté à l'application");
            notifcationBody.put("message", "");   //Enter your notification message
            notification.put("to", topic);
            notification.put("data", notifcationBody);
            Log.e("TAG", "try");
        } catch (JSONException e) {
            Log.e("TAG", "onCreate: " + e.getMessage());
        }

        sendNotification(notification);

    }

    public void sendNotification(JSONObject notification) {
        RequestQueue queue = Volley.newRequestQueue(this.getApplicationContext());
        Log.e("TAG", "sendNotification");
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(FCM_API, notification,
                new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i("TAG", "onResponse:" + response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("TAG", "onErrorResponse:" + error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("Authorization", serverKey);
                params.put("Content-Type", contentType);
                return params;
            }
        };
        queue.add(jsonObjectRequest);
    }

}