package ch.hesso.remembeer.ui.brewery;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.lifecycle.ViewModelProviders;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.io.IOException;

import ch.hesso.remembeer.BaseActivity;
import ch.hesso.remembeer.R;
import ch.hesso.remembeer.database.entity.BeerEntity;
import ch.hesso.remembeer.database.entity.BreweryEntity;
import ch.hesso.remembeer.utils.Helpers;
import ch.hesso.remembeer.utils.OnAsyncEventListener;
import ch.hesso.remembeer.viewmodel.beer.BeerViewModel;
import ch.hesso.remembeer.viewmodel.brewery.BreweryViewModel;

public class BreweryEditActivity extends BaseActivity {
    private static final String TAG = "BreweryEditActivity";

    private String breweryId;
    private BreweryEntity brewery = null;
    private EditText edit_name, edit_desc, edit_from, edit_adress, edit_web;
    private BreweryViewModel breweryViewModel;
    private ImageView brewery_photo;
    // private boolean createBrewery = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brewery_edit);
        getLayoutInflater().inflate(R.layout.activity_brewery_edit, frameLayout);
        setTitle(getString(R.string.brewery_edit));

        edit_name = findViewById(R.id.edit_name);
        edit_desc = findViewById(R.id.edit_desc);
        edit_from = findViewById(R.id.edit_from);
        edit_adress = findViewById(R.id.edit_adress);
        edit_web = findViewById(R.id.edit_web);
        brewery_photo = findViewById(R.id.brewery_photo);


        navigationView.setCheckedItem(position);
        breweryId = getIntent().getStringExtra("breweryId");

        if(breweryId == null) {
            brewery = new BreweryEntity();
            breweryId = "";
        }

        Log.d(TAG, "clicked on breweryId: " + breweryId);

        BreweryViewModel.Factory factory = new BreweryViewModel.Factory(getApplication(), breweryId);
        breweryViewModel = ViewModelProviders.of(this, factory).get(BreweryViewModel.class);
        breweryViewModel.getBrewery().observe(this, brewery -> {
            if(brewery != null) {
                this.brewery = brewery;
                initViewData();
            }
        });
    }

    public void initViewData() {
        edit_name.setText(brewery.getName());
        edit_from.setText(brewery.getCity());
        edit_desc.setText(brewery.getDescription());
        edit_web.setText(brewery.getWeb());
        edit_adress.setText(brewery.getAddress());
        initImageBrewery();
    }

    private void initImageBrewery() {
        Helpers.setImageFromFirebaseOrDefault(brewery_photo, brewery.getImage(), brewery,this);
    }

    private void updateBreweryImage(String path) {
        brewery.setImage(path);
//        breweryViewModel.update(brewery, new OnAsyncEventListener() {
//            @Override
//            public void onSuccess() {
//                Log.d(TAG, "updateBrewery: success");
//            }
//
//            @Override
//            public void onFailure(Exception e) {
//                Log.d(TAG, "updateBrewery: fail");
//            }
//        });
        initImageBrewery();
    }

    private boolean isFormValid() {
        boolean failed = false;
        if(edit_name.getText().toString().trim().length() == 0 ) {
            failed = true;
            edit_name.setError( "Valeur obligatoire" );
        }
        return !failed;
    }


    public void saveCliqued(View view) {
        Log.d("BreweryEditActivity", "cliqued");

        if(isFormValid()){
            brewery.setName(edit_name.getText().toString());
            brewery.setDescription(edit_desc.getText().toString());
            brewery.setCity(edit_from.getText().toString());
            brewery.setAddress(edit_adress.getText().toString());
            brewery.setWeb(edit_web.getText().toString());
            //TODO : sauvegarder l'image
            // brewery.setImage(brewery_photo);
            if(breweryId == null || breweryId.equals("")) {
                breweryViewModel.createBrewery(brewery, new OnAsyncEventListener() {
                    @Override
                    public void onSuccess() {
                        Log.d(TAG, "createBrewery: success");
                    }

                    @Override
                    public void onFailure(Exception e) {
                        Log.d(TAG, "createBrewery: fail");
                    }
                });
            } else {
                breweryViewModel.update(brewery, new OnAsyncEventListener() {
                    @Override
                    public void onSuccess() {
                        Log.d(TAG, "updateBrewery: success");
                    }

                    @Override
                    public void onFailure(Exception e) {
                        Log.d(TAG, "updateBrewery: fail");
                    }
                });
            }
            finish();
        }
    }

    public void changePicture(View view) {
        final CharSequence[] options = { "Prendre une photo", "Choisir depuis la gallerie","Annuler" };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choisis ton image");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Prendre une photo")) {
                    Intent takePicture = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePicture, 0);

                } else if (options[item].equals("Choisir depuis la gallerie")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, 1);

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        OnCompleteListener<Uri> onCompleteListener = new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                Log.d("onCompleteListener", task.getResult().getLastPathSegment());
                updateBreweryImage(task.getResult().getLastPathSegment());
            }
        };
        if (resultCode != RESULT_CANCELED) {
            switch (requestCode) {
                case 0:
                    if (resultCode == RESULT_OK && data != null) {
                        Bitmap image = (Bitmap) data.getExtras().get("data");
                        Helpers.saveBitmapToFirebase(image, onCompleteListener);
                    }
                    break;
                case 1:
                    if (resultCode == RESULT_OK && data != null) {
                        try {
                            Uri imageUri = data.getData();
                            Bitmap image = MediaStore.Images.Media.getBitmap(this.getContentResolver(),
                                    imageUri);
                            Helpers.saveBitmapToFirebase(image, onCompleteListener);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
            }

        }

    }
}