package ch.hesso.remembeer.viewmodel.beer;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import java.util.List;

import ch.hesso.remembeer.RemembeerApp;
import ch.hesso.remembeer.database.entity.BeerEntity;
import ch.hesso.remembeer.database.entity.BreweryEntity;
import ch.hesso.remembeer.database.repository.BeerRepository;
import ch.hesso.remembeer.utils.OnAsyncEventListener;
/**
 * Lien entre l'activité et le repository pour un objet unique
 */

public class BeerViewModel extends AndroidViewModel {

    private String beerId;
    private Application application;
    private BeerRepository beerRepository;
    private final MediatorLiveData<BeerEntity> observableBeer;

    public BeerViewModel(@NonNull Application application, String beerId,
                         BeerRepository beerRepository) {
        super(application);
        this.beerId = beerId;
        this.application = application;
        this.beerRepository = beerRepository;
        this.observableBeer = new MediatorLiveData<>();
        init();
    }
    private void init() {
        this.observableBeer.setValue(null);
        this.observableBeer.setValue(null);
    }

    public LiveData<BeerEntity> getBeer() {
        observableBeer.addSource(beerRepository.getById(this.beerId),
                observableBeer::setValue);
        return observableBeer;
    }

    public void createBeer(BeerEntity beer, OnAsyncEventListener callback) {
        ((RemembeerApp) getApplication()).getBeerRepository()
                .insert(beer, callback);
    }

    public void update(BeerEntity beer, OnAsyncEventListener callback) {
        ((RemembeerApp) getApplication()).getBeerRepository()
                .update(beer, callback);
    }

    public void delete(BeerEntity beer, OnAsyncEventListener callback) {
        ((RemembeerApp) getApplication()).getBeerRepository()
                .delete(beer, callback);
    }


    /**
     * Creer une instance du ViewModel
     */

    public static class Factory extends ViewModelProvider.NewInstanceFactory {

        @NonNull
        private final Application application;
        private final BeerRepository repository;
        private String beerId;

        public Factory(@NonNull Application application, String beerId) {
            this.application = application;
            this.beerId = beerId;
            repository = ((RemembeerApp) application).getBeerRepository();
        }

        @Override
        public <T extends ViewModel> T create(Class<T> modelClass) {
            return (T) new BeerViewModel(application, beerId, repository);
        }
    }
}
