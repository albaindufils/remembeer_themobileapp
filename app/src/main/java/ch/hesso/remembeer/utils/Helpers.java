package ch.hesso.remembeer.utils;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import ch.hesso.remembeer.R;
import ch.hesso.remembeer.database.entity.BeerEntity;

public class Helpers {

    public static void setImageFromFirebaseOrDefault(ImageView img, String path, Object item,
                                                     Context context) {
        StorageReference mStorageRef = FirebaseStorage.getInstance().getReference();
        if(path == null || path.equals("")) {
            path="/test.png";
        }
        StorageReference imgRef = mStorageRef.child(path);
        imgRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Log.d("getDownloadUrl",
                        "onSuccess: " + uri.toString());
                Glide.with(context)
                    .load(uri)
                    .into(img);
            }
        }).addOnFailureListener(exception -> {
            // Log.d("getDownloadUrl", "onFailure: " + exception.getMessage());
            img.setImageResource(0);
            if(item.getClass() == BeerEntity.class) {
                img.setImageResource(R.drawable.beer_default);
            } else {
                img.setImageResource(R.drawable.brasserie_default);
            }

        });
    }



    @RequiresApi(api = Build.VERSION_CODES.N)
    public static String saveBitmapToFirebase(Bitmap bitmapImage, OnCompleteListener<Uri> onCompleteListener) {
        String datetime = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        StorageReference mStorageRef = FirebaseStorage.getInstance().getReference();
        String pathToImgRef = "/images/" + UUID.randomUUID().toString() + "_" + datetime +
                "_import.png";
        StorageReference imgRef = mStorageRef.child(pathToImgRef);

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] data = byteArrayOutputStream.toByteArray();

        imgRef.putBytes(data).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                // Uri downloadUrl = taskSnapshot.getDownloadUrl();

            }
        }).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception
            {
                if (!task.isSuccessful())
                {
                    throw task.getException();
                }
                return imgRef.getDownloadUrl();
            }
        }).addOnCompleteListener(onCompleteListener);
        return pathToImgRef;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public static String saveBitmapToInternalStorage(Bitmap bitmapImage, Context context){
        ContextWrapper cw = new ContextWrapper(context);
        String datetime = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File directory = new File(cw.getDataDir() + "/images");
        if(!directory.exists()) {
            directory.mkdir();
        }
        File path = new File(directory,datetime+"_import.png");

        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(path);
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fileOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return path.getAbsolutePath();
    }

    public static void setImageFromPathOrDefault(ImageView image, String path, Object item) {
//        File imgFile = null;
//        try {
//            imgFile = new File(Helpers.setImageFromFirebaseOrDefault(image, path, new Object()));
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        if(!imgFile.exists()) {
////            Bitmap bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
////            image.setImageBitmap(bitmap);
////        } else {
//            image.setImageResource(0);
//            if(item.getClass() == BeerEntity.class) {
//                image.setImageResource(R.drawable.beer_default);
//            } else {
//                image.setImageResource(R.drawable.brasserie_default);
//
//            }
//        }
    }
}
