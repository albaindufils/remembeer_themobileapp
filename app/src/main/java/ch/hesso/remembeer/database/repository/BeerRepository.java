package ch.hesso.remembeer.database.repository;

import androidx.lifecycle.LiveData;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

import ch.hesso.remembeer.database.entity.BeerEntity;
import ch.hesso.remembeer.database.firebase.BeerListLiveData;
import ch.hesso.remembeer.database.firebase.BeerLiveData;
import ch.hesso.remembeer.utils.OnAsyncEventListener;

/**
 * Classe qui fait le lien entre la DAO et la ViewModel
 * pour les bieres
 */
public class BeerRepository {
    private static BeerRepository instance;

    private BeerRepository() { }

    public static BeerRepository getInstance() {
        if (instance == null) {
            synchronized (BeerRepository.class) {
                if (instance == null) {
                    instance = new BeerRepository();
                }
            }
        }
        return instance;
    }


    public BeerLiveData getById(final String id){
        DatabaseReference reference = FirebaseDatabase.getInstance()
                .getReference("beers")
                .child(id);
        return new BeerLiveData(reference);
    }

    public LiveData<List<BeerEntity>> getAll() {
        DatabaseReference reference = FirebaseDatabase.getInstance()
                .getReference("beers");
        return new BeerListLiveData(reference);
    }

    public void insert(final BeerEntity beer, final OnAsyncEventListener callback) {
        String id = FirebaseDatabase.getInstance().getReference("beers").push().getKey();
        FirebaseDatabase.getInstance()
                .getReference("beers")
                .child(id)
                .setValue(beer.toMap(), (databaseError, databaseReference) -> {
                    if (databaseError != null) {
                        callback.onFailure(databaseError.toException());
                    } else {
                        callback.onSuccess();
                    }
                });
    }

    public void update(final BeerEntity beer, final OnAsyncEventListener callback) {
        FirebaseDatabase.getInstance()
                .getReference("beers")
                .child(beer.getIdBeer())
                .updateChildren(beer.toMap(), (databaseError, databaseReference) -> {
                    if (databaseError != null) {
                        callback.onFailure(databaseError.toException());
                    } else {
                        callback.onSuccess();
                    }
                });
    }

    public void delete(final BeerEntity beer, final OnAsyncEventListener callback) {
        FirebaseDatabase.getInstance()
                .getReference("beers")
                .child(beer.getIdBeer())
                .removeValue((databaseError, databaseReference) -> {
                    if (databaseError != null) {
                        callback.onFailure(databaseError.toException());
                    } else {
                        callback.onSuccess();
                    }
                });
    }

}
