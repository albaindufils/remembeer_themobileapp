package ch.hesso.remembeer.database.repository;

import android.app.Application;
import androidx.lifecycle.LiveData;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import java.util.List;
import ch.hesso.remembeer.database.entity.BreweryEntity;
import ch.hesso.remembeer.database.firebase.BreweryListLiveData;
import ch.hesso.remembeer.database.firebase.BreweryLiveData;
import ch.hesso.remembeer.utils.OnAsyncEventListener;

/**
 * Classe qui fait le lien entre la DAO et la ViewModel
 * pour les brasseries
 */
public class BreweryRepository {

    private static BreweryRepository instance;

    private BreweryRepository() { }

    public static BreweryRepository getInstance() {
        if (instance == null) {
            synchronized (BeerRepository.class) {
                if (instance == null) {
                    instance = new BreweryRepository();
                }
            }
        }
        return instance;
    }

    public LiveData<BreweryEntity> getById(final String id) {
        DatabaseReference reference = FirebaseDatabase.getInstance()
                .getReference("breweries")
                .child(id);
        return new BreweryLiveData(reference);
    }

    public LiveData<List<BreweryEntity>> getAll() {
        DatabaseReference reference = FirebaseDatabase.getInstance()
                .getReference("breweries");
        return new BreweryListLiveData(reference);
    }

    public void insert(final BreweryEntity brewery, final OnAsyncEventListener callback) {
        String id = FirebaseDatabase.getInstance().getReference("breweries").push().getKey();
        FirebaseDatabase.getInstance()
                .getReference("breweries")
                .child(id)
                .setValue(brewery.toMap(), (databaseError, databaseReference) -> {
                    if (databaseError != null) {
                        callback.onFailure(databaseError.toException());
                    } else {
                        callback.onSuccess();
                    }
                });
    }

    public void update(final BreweryEntity brewery, final OnAsyncEventListener callback) {
        FirebaseDatabase.getInstance()
                .getReference("breweries")
                .child(brewery.getIdBrewery())
                .updateChildren(brewery.toMap(), (databaseError, databaseReference) -> {
                    if (databaseError != null) {
                        callback.onFailure(databaseError.toException());
                    } else {
                        callback.onSuccess();
                    }
                });
    }

    public void delete(final BreweryEntity brewery, final OnAsyncEventListener callback) {
        FirebaseDatabase.getInstance()
                .getReference("breweries")
                .child(brewery.getIdBrewery())
                .removeValue((databaseError, databaseReference) -> {
                    if (databaseError != null) {
                        callback.onFailure(databaseError.toException());
                    } else {
                        callback.onSuccess();
                    }
                });
    }
}
