package ch.hesso.remembeer.database.firebase;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import ch.hesso.remembeer.database.entity.BreweryEntity;

public class BreweryListLiveData extends LiveData<List<BreweryEntity>> {
    private static final String TAG = "BreweryListLiveData";

    private final DatabaseReference reference;
    private final MyValueEventListener listener = new MyValueEventListener();

    public BreweryListLiveData(DatabaseReference ref) {
        this.reference = ref;
    }

    @Override
    protected void onActive() {
        Log.d(TAG, "onActive");
        reference.addValueEventListener(listener);
    }

    @Override
    protected void onInactive() {
        Log.d(TAG, "onInactive");
        super.onInactive();
    }

    private class MyValueEventListener implements ValueEventListener {
        @Override
        public void onDataChange(@NonNull DataSnapshot snapshot) {
            setValue(toBreweriesList(snapshot));
        }

        @Override
        public void onCancelled(@NonNull DatabaseError error) {
            Log.e(TAG, "Can't listen to query " + reference, error.toException());
        }
    }

    private List<BreweryEntity> toBreweriesList(DataSnapshot dataSnapshot) {
        List<BreweryEntity> breweries = new ArrayList<>();
        for (DataSnapshot brewerySnapshot :
                dataSnapshot.getChildren()) {

            BreweryEntity brewery = brewerySnapshot.getValue(BreweryEntity.class);
            brewery.setIdBrewery(brewerySnapshot.getKey());
            breweries.add(brewery);

        }
        return breweries;
    }
}
