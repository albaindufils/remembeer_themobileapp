package ch.hesso.remembeer.database.firebase;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import ch.hesso.remembeer.database.entity.BreweryEntity;

public class BreweryLiveData extends LiveData<BreweryEntity> {
    private static final String TAG = "BreweryLiveData";

    private final DatabaseReference reference;
    private final MyValueEventListener listener = new MyValueEventListener();

    public BreweryLiveData(DatabaseReference ref) {
        this.reference = ref;
    }
    @Override
    protected void onActive() {
        Log.d(TAG, "onActive");
        reference.addValueEventListener(listener);
    }

    @Override
    protected void onInactive() {
        Log.d(TAG, "onInactive");
        super.onInactive();
    }

    private class MyValueEventListener implements ValueEventListener {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            if (dataSnapshot.exists()) {
                BreweryEntity entity = dataSnapshot.getValue(BreweryEntity.class);
                entity.setIdBrewery(dataSnapshot.getKey());
                setValue(entity);
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError error) {
            Log.e(TAG, "Cant listen to query " + reference, error.toException());
        }
    }
}