package ch.hesso.remembeer.database.firebase;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import ch.hesso.remembeer.database.entity.BeerEntity;

public class BeerListLiveData extends LiveData<List<BeerEntity>> {
    private static final String TAG = "BeerListLiveData";

    private final DatabaseReference reference;
    private final MyValueEventListener listener = new MyValueEventListener();

    public BeerListLiveData(DatabaseReference ref) {
        this.reference = ref;
    }

    @Override
    protected void onActive() {
        Log.d(TAG, "onActive");
        reference.addValueEventListener(listener);
    }

    @Override
    protected void onInactive() {
        Log.d(TAG, "onInactive");
        super.onInactive();
    }

    private class MyValueEventListener implements ValueEventListener {
        @Override
        public void onDataChange(@NonNull DataSnapshot snapshot) {
            setValue(toBeersList(snapshot));
        }

        @Override
        public void onCancelled(@NonNull DatabaseError error) {
            Log.e(TAG, "Can't listen to query " + reference, error.toException());
        }
    }

    private List<BeerEntity> toBeersList(DataSnapshot dataSnapshot) {
        List<BeerEntity> beers = new ArrayList<>();
        for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                BeerEntity beer = childSnapshot.getValue(BeerEntity.class);
                beer.setIdBeer(childSnapshot.getKey());
                beers.add(beer);
            }

        return beers;
    }
}

