package ch.hesso.remembeer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.List;

import ch.hesso.remembeer.R;
import ch.hesso.remembeer.database.entity.BreweryEntity;
import ch.hesso.remembeer.ui.beer.BeerEditActivity;

public class BreweryAdapter extends ArrayAdapter<BreweryEntity> {

    Context context;
    private int res;
    List<BreweryEntity> data;
    AutoCompleteTextView breweryDropdown;

    public BreweryAdapter(Context context, int res, List<BreweryEntity> data,
                          AutoCompleteTextView breweryDropdown) {
        super(context, res, data);
        this.res = res;
        this.context = context;
        this.data = data;
        this.breweryDropdown = breweryDropdown;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        try{


            if(convertView==null){
                // inflate the layout
                LayoutInflater inflater = ((BeerEditActivity) context).getLayoutInflater();
                convertView = inflater.inflate(res, parent, false);
            }

            // object item based on the position
            BreweryEntity brewery = data.get(position);

            // get the TextView and then set the text (item name) and tag (item ID) values
            TextView textViewItem = (TextView) convertView.findViewById(R.id.tv_dropdown_brewery_item);
            textViewItem.setText(brewery.getName());


        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;

    }
}
